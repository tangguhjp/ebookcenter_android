package jp.tanggo.e_bookcenter;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import jp.tanggo.e_bookcenter.ApiHelper.UtilsApi;

/**
 * Created by me on 25/06/18.
 */

public class BooksAdapter extends RecyclerView.Adapter<BooksAdapter.ViewHolder>{
    private static final String BASE_URL = UtilsApi.BASE_URL_DATA;

    List<Books> list;
    private Context context;


    public BooksAdapter(List<Books> list, Context context) {
        this.list = list;
        this.context = context;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.rowlayout_books, parent, false);
        return new ViewHolder(v);
    }


    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Books books = list.get(position);
        holder.tv_title.setText(books.getTitle());
        holder.tv_author.setText("by : "+books.getAuthor());
        Picasso.with(context)
                .load(BASE_URL+books.getCover())
                .into(holder.iv_cover);

        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // open another activity on item click
                Intent intent = new Intent(context, BookDetails.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable("data", books);
                intent.putExtras(bundle);
                context.startActivity(intent); // start Intent
            }
        });
    }

    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_title, tv_author;
        private ImageView iv_cover;
        public ViewHolder(View itemView) {
            super(itemView);
            this.iv_cover = itemView.findViewById(R.id.iv_cover);
            this.tv_title = itemView.findViewById(R.id.tv_title);
            this.tv_author = itemView.findViewById(R.id.tv_author);
        }
    }

}
