package jp.tanggo.e_bookcenter;

/**
 * Created by me on 26/06/18.
 */

public class Comments {
    int bookId;
    int commentId;
    String value;
    String username;

    public Comments(int bookId, int commentId, String value, String username) {
        this.bookId = bookId;
        this.commentId = commentId;
        this.value = value;
        this.username = username;
    }

    public Comments() {

    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public int getCommentId() {
        return commentId;
    }

    public void setCommentId(int commentId) {
        this.commentId = commentId;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
}
