package jp.tanggo.e_bookcenter;


import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.squareup.picasso.Picasso;


import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import jp.tanggo.e_bookcenter.ApiHelper.BaseApiService;
import jp.tanggo.e_bookcenter.ApiHelper.UtilsApi;
import okhttp3.ResponseBody;
import pub.devrel.easypermissions.EasyPermissions;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by me on 25/06/18.
 */

public class BookDetails extends AppCompatActivity implements EasyPermissions.PermissionCallbacks{
    TextView tv_title, tv_author, tv_year, tv_description;
    ImageView iv_cover, iv_addComment;
    Button read;
    EditText comment_value ;
    private static final String BASE_URL = UtilsApi.BASE_URL_DATA;

    private static final int WRITE_REQUEST_CODE = 300;
    private static final String TAG = MainActivity.class.getSimpleName();
    private String url, bookTitle;
    private int book_id, alert = 1;

    private RecyclerView recyclerView;
    CommentAdapter commentAdapter;
    List<Comments> listing;

    BaseApiService mApiService;

    //TEST
    final Context context = this;
    private Button button;
    private EditText result;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details);
        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Book Details");

        //TEST

        iv_addComment = findViewById(R.id.iv_addComment);

        iv_addComment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        context);

                // set prompts.xml to alertdialog builder
                LayoutInflater li = LayoutInflater.from(context);

                View promptsView = li.inflate(R.layout.add_comment, null);

                alertDialogBuilder.setView(promptsView);
                comment_value = promptsView.findViewById(R.id.et_Dialog);

                alertDialogBuilder
                        .setCancelable(false)
                        .setPositiveButton("Send",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        addComment();
                                        Toast.makeText(getApplicationContext(), "Comment has been sent", Toast.LENGTH_LONG).show();
                                    }
                                })
                        .setNegativeButton("Cancel",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog,int id) {
                                        dialog.cancel();
                                    }
                                });

                AlertDialog alertDialog = alertDialogBuilder.create();

                alertDialog.show();
                alert = 0;
            }
        });

        //TEST


        recyclerView = findViewById(R.id.rv_comment);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 1);
        recyclerView.setLayoutManager(gridLayoutManager);

        listing = new ArrayList<>();
        mApiService = UtilsApi.getAPIService();

        initComponents();

        Bundle bundle = getIntent().getExtras();

        final Books book = bundle.getParcelable("data");
        tv_title = findViewById(R.id.tvBookTitle);
        tv_author = findViewById(R.id.tvBookAuthor);
        tv_year = findViewById(R.id.tvBookYears);
        tv_description = findViewById(R.id.tvBookSinopsis);
        iv_cover = findViewById(R.id.ivBookCover);
        read = findViewById(R.id.btRead);

        bookTitle = book.getTitle();
        book_id = book.bookId;

        url = BASE_URL+book.getFile();
        tv_title.setText(book.getTitle());
        tv_author.setText(book.getAuthor());
        tv_year.setText(String.valueOf(book.getYear()));
        tv_description.setText(book.getDescription());
        Picasso.with(this)
                .load(BASE_URL+book.getCover())
                .into(iv_cover);

        read.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (CheckForSDCard.isSDCardPresent()) {

                    //check if app has permission to write to the external storage.
                    if (EasyPermissions.hasPermissions(BookDetails.this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                        //Get the URL entered
                        new DownloadFile().execute(url);

                    } else {
                        //If permission is not present request for the same.
                        EasyPermissions.requestPermissions(BookDetails.this, getString(R.string.write_file), WRITE_REQUEST_CODE, Manifest.permission.READ_EXTERNAL_STORAGE);
                    }


                } else {
                    Toast.makeText(getApplicationContext(),
                            "SD Card not found", Toast.LENGTH_LONG).show();

                }
//
            }
        });
    }

    protected void initComponents(){
        Call<ResponseBody> call = mApiService.getComment();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,@NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        JSONArray jsonRESULTS = new JSONArray(response.body().string());
                        if (jsonRESULTS.length()>0){
                            Comments comments;
                            for (int i= 0; i<jsonRESULTS.length();i++){
                                comments = new Comments();
                                JSONObject jsonObject = (JSONObject) jsonRESULTS.get(i);
                                if (jsonObject.getInt("book_id")==book_id){
                                    comments.setBookId(jsonObject.getInt("id"));
                                    comments.setCommentId(jsonObject.getInt("book_id"));
                                    comments.setValue(jsonObject.getString("value"));
                                    comments.setUsername(jsonObject.getString("username"));
                                    listing.add(comments);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    int viewHeight = 100 * listing.size();
                    recyclerView.getLayoutParams().height = viewHeight;
                    commentAdapter = new CommentAdapter(listing, getApplicationContext());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(commentAdapter);
                }
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }

    protected void addComment(){
        Call<ResponseBody> call = mApiService.addComment(book_id, UtilsApi.username, comment_value.getText().toString() );
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,@NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        listing.clear();
                        initComponents();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }

            /**
             * Invoked when a network exception occurred talking to the server or when an unexpected
             * exception occurred creating the request or processing the response.
             *
             * @param call
             * @param t
             */
            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        EasyPermissions.onRequestPermissionsResult(requestCode, permissions, grantResults, BookDetails.this);
    }

    @Override
    public void onPermissionsGranted(int requestCode, List<String> perms) {
        //Download the file once permission is granted
        new DownloadFile().execute(url);
    }

    @Override
    public void onPermissionsDenied(int requestCode, List<String> perms) {
        Log.d(TAG, "Permission has been denied");
    }

    public void comment() {
    }

    /**
     * Async Task to download file from URL
     */
    private class DownloadFile extends AsyncTask<String, String, String> {

        private ProgressDialog progressDialog;
        private String fileName;
        private String folder;
        private boolean isDownloaded;

        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            this.progressDialog = new ProgressDialog(BookDetails.this);
            this.progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
            this.progressDialog.setCancelable(false);
            this.progressDialog.show();
        }

        /**
         * Downloading file in background thread
         */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection connection = url.openConnection();
                connection.connect();
                // getting file length
                int lengthOfFile = connection.getContentLength();


                // input stream to read file - with 8k buffer
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                String timestamp = new SimpleDateFormat("yyyy.MM.dd.HH.mm.ss").format(new Date());

                //Extract file name from URL
                fileName = f_url[0].substring(f_url[0].lastIndexOf('/') + 1, f_url[0].length());
                fileName = bookTitle+".pdf";
                //Append timestamp to file name
//                fileName = timestamp + "_" + fileName;

                //External directory path to save file
                folder = Environment.getExternalStorageDirectory() + File.separator + "E-book Center/";

                //Create androiddeft folder if it does not exist
                File directory = new File(folder);

                if (!directory.exists()) {
                    directory.mkdirs();
                }

                File checkFile = new File(folder+fileName);
                if (checkFile.exists()){
                    return "downloaded";
                }

                // Output stream to write file
                OutputStream output = new FileOutputStream(folder + fileName);

                byte data[] = new byte[1024];

                long total = 0;

                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress("" + (int) ((total * 100) / lengthOfFile));

                    // writing data to file
                    output.write(data, 0, count);
                }

                // flushing output
                output.flush();

                // closing streams
                output.close();
                input.close();

                openPdf();
                return "Downloaded at: " + folder + fileName;

            } catch (Exception e) {
                Log.e("Error: ", e.getMessage());
            }

            return "Something went wrong";
        }

        protected void openPdf(){
            if(Build.VERSION.SDK_INT>=24){
                try{
                    Method m = StrictMode.class.getMethod("disableDeathOnFileUriExposure");
                    m.invoke(null);
                }catch(Exception e){
                    e.printStackTrace();
                }
            }

            File pdfFile = new File(folder+fileName);
            Uri path = Uri.fromFile(pdfFile);
            Intent pdfIntent = new Intent(Intent.ACTION_VIEW);
            pdfIntent.setDataAndType(path, "application/pdf");
            pdfIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);

            try{
                startActivity(pdfIntent);
            }catch(ActivityNotFoundException e){
                Toast.makeText(getApplicationContext(), "No Application available to view PDF", Toast.LENGTH_SHORT).show();
            }
        }

        /**
         * Updating progress bar
         */
        protected void onProgressUpdate(String... progress) {
            // setting progress percentage
            progressDialog.setProgress(Integer.parseInt(progress[0]));
        }

        public void comment(){
            Toast.makeText(context, "INI", Toast.LENGTH_LONG).show();
        }

        @Override
        protected void onPostExecute(String message) {
            // dismiss the dialog after the file was downloaded
            this.progressDialog.dismiss();

            // Display File path after downloading
            if (message.equals("downloaded")){
                AlertDialog.Builder builder = new
                        AlertDialog.Builder(BookDetails.this);
                builder.setTitle(fileName+" Previously has been downloaded")
                        .setMessage("Do you want to open the file?")
                        .setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                openPdf();
//                                    Toast.makeText(getApplicationContext(), "Data Successfully Deleted", Toast.LENGTH_LONG).show();
                            }
                        })
                        .setNegativeButton(android.R.string.no, new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                            }
                        })
                        .setIcon(android.R.drawable.ic_dialog_alert)
                        .show();
            }else {
                Toast.makeText(getApplicationContext(),
                        message, Toast.LENGTH_LONG).show();
            }
        }
    }

}
