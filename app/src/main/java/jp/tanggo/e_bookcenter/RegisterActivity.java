package jp.tanggo.e_bookcenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import jp.tanggo.e_bookcenter.ApiHelper.BaseApiService;
import jp.tanggo.e_bookcenter.ApiHelper.UtilsApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by me on 03/06/18.
 */

public class RegisterActivity extends AppCompatActivity{
    private static final String TAG = "RegisterActivity";
    EditText fullname, username, email, password;
    Button bt_regist;
    ProgressDialog pDialog;

    Context mContext;
    BaseApiService mApiService;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        mContext = this;
        mApiService = UtilsApi.getAPIService();
        initComponents();
    }

    private void initComponents() {
        fullname = (EditText) findViewById(R.id.et_fullname_regist);
        username = (EditText) findViewById(R.id.et_username_regist);
        email = (EditText) findViewById(R.id.et_email_regist);
        password = findViewById(R.id.et_pass_regist);
        bt_regist = findViewById(R.id.bt_regist);
        
        bt_regist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = ProgressDialog.show(mContext,null, "Please wait...", true,false);
                requestRegister();
            }
        });

    }

    private void requestRegister() {
        mApiService.registerRequest(fullname.getText().toString(),
                username.getText().toString(),
                email.getText().toString(),
                password.getText().toString(),
                2).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if (response.isSuccessful()) {
                    pDialog.dismiss();
                    try {
                        JSONObject jsonRESULTS = new JSONObject(response.body().string());
                        if (jsonRESULTS.getString("message").equals("true")) {
                            //Jika Login Sukses, Maka data yang ada di response API akan
                            //diparsing ke activity berikutnya
                            Toast.makeText(mContext, "REGISTER SUCCES", Toast.LENGTH_SHORT).show();
                            //String nama = jsonRESULTS.getJSONObject("user").getString("name");
                            Intent intent = new Intent(mContext, LoginActivity.class);
                            startActivity(intent);
                        } else {
                            //Jika Login Gagal
                            String error_message = jsonRESULTS.getString("message");
                            Toast.makeText(mContext, error_message, Toast.LENGTH_SHORT).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                } else {
                    Log.i("debug", "onResponse: FAILED");
                    pDialog.dismiss();
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                
            }
        });
    }
}
