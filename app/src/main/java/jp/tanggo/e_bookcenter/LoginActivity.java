package jp.tanggo.e_bookcenter;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import jp.tanggo.e_bookcenter.ApiHelper.BaseApiService;
import jp.tanggo.e_bookcenter.ApiHelper.UtilsApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by me on 03/06/18.
 */

public class LoginActivity  extends AppCompatActivity{
    ProgressDialog pDialog;
    Button bt_reg, bt_login;
    EditText txt_username, txt_password;

    Context mContext;
    BaseApiService mApiService;

    SharedPreferences sharedPreferences;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        mContext = this;
        mApiService = UtilsApi.getAPIService(); //Inisialisasi isi package ApiHelper
        initComponents();
    }

    private void initComponents() {
        txt_username = (EditText) findViewById(R.id.txt_user_login);
        txt_password = (EditText) findViewById(R.id.txt_pass_login);
        bt_login = (Button) findViewById(R.id.bt_login);
        bt_reg = (Button) findViewById(R.id.bt_reg);


        bt_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pDialog = ProgressDialog.show(mContext, null, "Please wait...", true, false);
                requestLogin();
            }
        });

        bt_reg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(mContext, RegisterActivity.class));
            }
        });

    }

    /**
     * Take care of popping the fragment back stack or finishing the activity
     * as appropriate.
     */
    @Override
    public void onBackPressed() {
        finish();
    }

    private void requestLogin() {
        mApiService.loginRequest(txt_username.getText().toString(), txt_password.getText().toString())
                .enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                        if (response.isSuccessful()) {
                            pDialog.dismiss();
                            try {
                                JSONObject jsonRESULTS = new JSONObject(response.body().string());
                                if (jsonRESULTS.getString("error").equals("false")) {
                                    UtilsApi.username = txt_username.getText().toString();
                                    Toast.makeText(mContext, "LOGIN SUCCES", Toast.LENGTH_SHORT).show();
                                    Intent intent = new Intent(mContext, MainActivity.class);
                                    startActivity(intent);
                                } else {
                                    Toast.makeText(mContext, "FAILED", Toast.LENGTH_SHORT).show();
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }else{
                            pDialog.dismiss();
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {
                        Log.e("debug","onFailure: ERROR > " + t.toString());
                        pDialog.dismiss();

                    }
                });
    }
}
