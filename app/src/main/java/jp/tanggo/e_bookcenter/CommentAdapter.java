package jp.tanggo.e_bookcenter;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;
import jp.tanggo.e_bookcenter.ApiHelper.UtilsApi;

/**
 * Created by me on 26/06/18.
 */

public class CommentAdapter extends RecyclerView.Adapter<CommentAdapter.ViewHolder>{
    private static final String TAG = "CommentAdapter";
    private static final String BASE_URL = UtilsApi.BASE_URL_DATA;

    List<Comments> list;
    private Context context;

    public CommentAdapter(List<Comments> list, Context context) {
        this.list = list;
        this.context = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.comment, parent, false);
        return new ViewHolder(v);
    }



    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Comments comment = list.get(position);
        holder.tv_value.setText(comment.getValue());
        holder.tv_username.setText(comment.getUsername());

        if (comment.getUsername().equals(UtilsApi.username)){
            RelativeLayout.LayoutParams adminTextViewLayoutParams = new RelativeLayout.LayoutParams(
                    RelativeLayout.LayoutParams.WRAP_CONTENT, RelativeLayout.LayoutParams.WRAP_CONTENT);
            adminTextViewLayoutParams.addRule(RelativeLayout.ALIGN_PARENT_RIGHT);
            holder.tv_username.setLayoutParams(adminTextViewLayoutParams);
            holder.tv_username.setBackgroundResource(R.drawable.button_shape2);
            holder.tv_username.setText("You");
        }
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
            }

        });
    }
    @Override
    public int getItemCount() {
        return list.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        private TextView tv_value, tv_username;
        public ViewHolder(View itemView) {
            super(itemView);
            this.tv_value = itemView.findViewById(R.id.tv_comment);
            this.tv_username = itemView.findViewById(R.id.tv_username_comment);
        }
    }

}
