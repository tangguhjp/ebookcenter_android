package jp.tanggo.e_bookcenter;

import android.app.ActionBar;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import jp.tanggo.e_bookcenter.ApiHelper.BaseApiService;
import jp.tanggo.e_bookcenter.ApiHelper.UtilsApi;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by me on 25/06/18.
 */
public class BooksListActivity extends AppCompatActivity {
    private static final String TAG = "BooksListActivity";
    public static int currentItem ;
    static View.OnClickListener bookOnClickListener;

    private RecyclerView recyclerView;
    BooksAdapter booksAdapter;
    List<Books> listing;

    BaseApiService mApiService;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_books);

        getSupportActionBar().setHomeButtonEnabled(true);
        getSupportActionBar().setTitle("Books Lists");

        bookOnClickListener = new BooksOnClickListener(this);
        recyclerView = findViewById(R.id.recyclerView_Books);
        //Grid
        GridLayoutManager gridLayoutManager = new GridLayoutManager(getApplicationContext(), 2);
        recyclerView.setLayoutManager(gridLayoutManager);

        listing = new ArrayList<>();
        mApiService = UtilsApi.getAPIService();

        initComponents();
    }

    private void initComponents() {
        Call<ResponseBody> call = mApiService.getBooks();
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(@NonNull Call<ResponseBody> call,@NonNull Response<ResponseBody> response) {
                if (response.isSuccessful()){
                    try {
                        JSONArray jsonRESULTS = new JSONArray(response.body().string());
                        if (jsonRESULTS.length()>0){
                            Books books;
                            for (int i= 0; i<jsonRESULTS.length();i++){
                                books = new Books();
                                JSONObject jsonObject = (JSONObject) jsonRESULTS.get(i);

                                books.setBookId(jsonObject.getInt("book_id"));
                                books.setTitle(jsonObject.getString("title"));
                                books.setAuthor(jsonObject.getString("author"));
                                books.setPublisher(jsonObject.getString("publisher"));
                                books.setYear(jsonObject.getInt("year"));
                                books.setCategory(jsonObject.getString("category"));
                                books.setDescription(jsonObject.getString("description"));
                                books.setFile(jsonObject.getString("file"));
                                books.setCover(jsonObject.getString("cover"));
                                listing.add(books);
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }

                    booksAdapter = new BooksAdapter(listing, getApplicationContext());
                    recyclerView.setItemAnimator(new DefaultItemAnimator());
                    recyclerView.setAdapter(booksAdapter);
                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {

            }

        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }
    private class BooksOnClickListener implements View.OnClickListener {
        private final Context context;

        private BooksOnClickListener(Context c) {
            this.context = c;
        }

        @Override
        public void onClick(View view) {
            currentItem = recyclerView.getChildAdapterPosition(view);
            Books books = listing.get(currentItem);

            Intent intent = new Intent(getApplicationContext(), BookDetails.class);
            Bundle bundle = new Bundle();
            bundle.putParcelable("data", books);
            intent.putExtras(bundle);
            startActivity(intent);
        }
    }


}
