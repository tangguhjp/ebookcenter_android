package jp.tanggo.e_bookcenter;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by me on 25/06/18.
 */

public class Books implements Parcelable {
    int bookId;
    String title;
    String author;
    String publisher;
    int year;
    String category;
    String description;
    String file;
    String cover;

    public Books(int bookId, String title, String author, String publisher, int year, String category, String description, String file, String cover) {
        this.bookId = bookId;
        this.title = title;
        this.author = author;
        this.publisher = publisher;
        this.year = year;
        this.category = category;
        this.description = description;
        this.file = file;
        this.cover = cover;
    }

    public Books() {

    }

    protected Books(Parcel in) {
        bookId = in.readInt();
        title = in.readString();
        author = in.readString();
        publisher = in.readString();
        year = in.readInt();
        category = in.readString();
        description = in.readString();
        file = in.readString();
        cover = in.readString();
    }

    public static final Creator<Books> CREATOR = new Creator<Books>() {
        @Override
        public Books createFromParcel(Parcel in) {
            return new Books(in);
        }

        @Override
        public Books[] newArray(int size) {
            return new Books[size];
        }
    };

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(String author) {
        this.author = author;
    }

    public String getPublisher() {
        return publisher;
    }

    public void setPublisher(String publisher) {
        this.publisher = publisher;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getCover() {
        return cover;
    }

    public void setCover(String cover) {
        this.cover = cover;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(bookId);
        parcel.writeString(title);
        parcel.writeString(author);
        parcel.writeString(publisher);
        parcel.writeInt(year);
        parcel.writeString(category);
        parcel.writeString(description);
        parcel.writeString(file);
        parcel.writeString(cover);
    }
}
