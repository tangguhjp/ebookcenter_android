package jp.tanggo.e_bookcenter.ApiHelper;

/**
 * Created by me on 03/06/18.
 */

public class UtilsApi {
    public static final String BASE_URL_API = "http://192.168.43.169:8888/api/";
    public static final String BASE_URL_DATA = "http://192.168.43.169:8000/"; //BELUM FIX

    public static String username = "";

    //Mendeklarasi Interface BaseApiService
    public static BaseApiService getAPIService(){
        return RetrofitClient.getClient(BASE_URL_API).create(BaseApiService.class);
    }
}
