package jp.tanggo.e_bookcenter.ApiHelper;

import android.content.Intent;

import java.io.UTFDataFormatException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

/**
 * Created by me on 03/06/18.
 */

public interface BaseApiService {
    String comment = "";

    @FormUrlEncoded
    @POST("user/login")
    Call<ResponseBody> loginRequest(@Field("username") String username,
                                    @Field("password") String password);
    //Fungsi ini untuk memanggil API http://localhost/backend/register.php
    @FormUrlEncoded
    @POST("user/register")
    Call<ResponseBody> registerRequest(@Field("name") String name,
                                       @Field("username") String username,
                                       @Field("email") String email,
                                       @Field("password") String password,
                                       @Field("role_id") Integer role_id);
    @GET("book/")
    Call<ResponseBody> getBooks();

    @GET("comment/")
    Call<ResponseBody> getComment();

    @FormUrlEncoded
    @POST("comment/create")
    Call<ResponseBody> addComment(@Field("book_id") Integer book_id,
                                  @Field("username") String username,
                                  @Field("value") String value);

    @FormUrlEncoded
    @POST("comment/delete/")
    Call<ResponseBody> deleteComment(@Field("id") Integer role_id);
}
