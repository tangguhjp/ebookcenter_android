package jp.tanggo.e_bookcenter;

import android.os.Environment;

/**
 * Created by me on 25/06/18.
 */

class CheckForSDCard {
    //Method to Check If SD Card is mounted or not
    public static boolean isSDCardPresent() {
        if (Environment.getExternalStorageState().equals(

                Environment.MEDIA_MOUNTED)) {
            return true;
        }
        return false;
    }
}
